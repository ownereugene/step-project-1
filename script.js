const serviceUl = document.querySelector(".service-section-nav-wrapper ul");
const serviceLi = Array.from(
  document.querySelectorAll(".service-section-nav-wrapper li")
);
const serviceDiv = Array.from(document.querySelectorAll(".service-menu-item"));

serviceUl.addEventListener("click", (event) => {
  serviceLi.forEach((el, index) => {
    el.classList.remove("list-active");
    el.dataset.li = index;
  });
  event.target.classList.add("list-active");

  serviceDiv.forEach((elem, index) => {
    elem.dataset.li = index;
    elem.classList.remove("active");
    elem.classList.add("not-active");
  });
  serviceDiv.forEach((elem) => {
    if (elem.dataset.li === event.target.dataset.li) {
      elem.classList.add("active");
    }
  });
});

//<------------------------buttonAmazing-------------------------->//

const gridItems = document.querySelectorAll(".grid-items");
const buttonAmazing = document.querySelector(".button-amazing");
function showsImg(){
  gridItems.forEach(image=>{
    if (image.classList.contains("not-active")){
      image.style.display = "block"; 
      image.classList.remove("not-active")
      buttonAmazing.style.display ="none";
    }
  } );
}
buttonAmazing.addEventListener("click",showsImg);

//<------------------------GAllERY-------------------------->//


const tabsWork = document.querySelectorAll('.tabs-li');
const workGrids = document.querySelectorAll('.grid-items');
tabsWork.forEach((tab) => {
  tab.addEventListener('click', () => {
    const activeTab = document.querySelector('.tabs-li.active-show');
    activeTab.classList.remove('active-show');
    tab.classList.add('active-show');
    buttonAmazing.style.display = 'none';
    const activeCategory = tab.textContent.toLowerCase().replace(' ', '-');
    let shownCount = 0; 
    workGrids.forEach((grid) => {
      grid.classList.add('not-active');
      if (grid.classList.contains(activeCategory) || activeCategory === 'all') {
        if (shownCount < 12) { 
          grid.classList.remove('not-active');
          shownCount++;
        } else {
          grid.classList.add('not-active');
          buttonAmazing.style.display = 'block';
        }
      }
    });
  });
});

/*<--What People Say About theHam-->*/

const ulPeopleSay = document.querySelector(".section-7 ul");
const liItemImg = Array.from(document.querySelectorAll(".section-7 li img"));
const articleItem = Array.from(
  document.querySelectorAll(".about-section-wrapper")
);
const leftArrow = document.querySelector(".left-arrow");
const rightArrow = document.querySelector(".right-arrow");

liItemImg.forEach((el, index) => {
  el.parentElement.dataset.index = index;
});
articleItem.forEach((elem, index) => {
  elem.dataset.index = index;
});

liItemImg.forEach((element) => {
  element.addEventListener("click", (event) => {
    liItemImg.forEach((el) => {
      el.parentElement.classList.remove("active-li");
    });
    event.target.parentElement.classList.add("active-li");
    articleItem.forEach((elem) => {
      elem.classList.add("about-hide");
    });
    articleItem.forEach((elem) => {
      if (elem.dataset.index === event.target.parentElement.dataset.index) {
        elem.classList.remove("about-hide");
      }
    });
  });
});

leftArrow.addEventListener("click", moveLeft);
rightArrow.addEventListener("click", moveRight);

function moveLeft() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === 0) {
      count = liItemImg.length;
    }
    if (Number(el.parentElement.dataset.index) === count - 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count - 1) {
      el.classList.remove("about-hide");
    }
  });
}

function moveRight() {
  let count = 0;
  liItemImg.forEach((el) => {
    if (el.parentElement.classList.contains("active-li")) {
      count = Number(el.parentElement.dataset.index);
      el.parentElement.classList.remove("active-li");
    }
  });
  liItemImg.forEach((el) => {
    if (count === liItemImg.length - 1) {
      count = -1;
    }
    if (Number(el.parentElement.dataset.index) === count + 1) {
      el.parentElement.classList.add("active-li");
    }
  });
  articleItem.forEach((el) => {
    el.classList.add("about-hide");
  });

  articleItem.forEach((el) => {
    if (Number(el.dataset.index) === count + 1) {
      el.classList.remove("about-hide");
    }
  });
}